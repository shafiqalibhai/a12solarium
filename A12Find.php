<?php

/**
 * @namespace
 */
namespace Solarium\Core\Client\Adapter;
use Solarium\Core\Configurable;
use Solarium\Core\Client\Response;
use Solarium\Core\Client\Endpoint;

define('A12_TMP_DEFAULT_SCHEME', 'https://');
define('A12_FIND_SERVER', 'find.axis12.com');

define('A12_VALIDATE_METHOD', 'a12_webservices.validate');

define('A12_FIND_VERSION', "7.x-1.2");

if (!defined('REQUEST_TIME')) {
  define('REQUEST_TIME', isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : time());
}

/****************************************
 *  Helper functions
 ****************************************/
/**
 *
 * @param timestamp
 * timestamp of  the host server
 *
 * @param string
 * unique identifier
 *
 * @param string
 * subscription key used as salt for hmac
 *
 * @param mixed array()
 * params that are to be passed on xmlrpc added to generate unique hash.
 *
 * @return string
 * base64 encoded string
 */
function a12_connect_hmac_hash($time, $salt, $magic_key) {
  $salt = substr(base_convert(sha1($salt), 16, 36), 0, 6);
  $data = $time . ':' . $salt . ':' . $magic_key;
  return base64_encode(hash_hmac('sha1', $data, $magic_key, TRUE));
}

function variable_get($var_name) {
  $key = '';
  switch($var_name) {
    case "a12_identifier":
        $key = a12_identifier;
        break;
    case "a12_key":
        $key = a12_key;
        break;
    case "a12_network_address":
        $key = A12_VALIDATE_METHOD;
        break;
    case "a12_subscription_data":
        $key = FALSE;
        break;
  }
  
  return $key;
}

/**
 * Prepares headers that shall be used by A12 webservices to validate
 * incoming requests
 *
 * @param aray $params
 * Any additional paramters that are to be used in preparing headers
 *
 * @return array
 */
function auth_headers($params = array()) {
  global $base_url;
  $a12_headers = array();
  $a12_headers['timestamp'] = $this->timestamp;
  $a12_headers['identifier'] = $this->a12_identifier;
  $a12_headers['site_url'] = $base_url;
  $a12_headers['site_name'] = $_SERVER['SERVER_NAME'];
  $a12_headers['ip'] = $_SERVER['SERVER_ADDR'];
  // Although we can always send the key to XMLRPC request as
  // $a12_headers['key'] = $this->a12_key;
  // We avoid sending key over and instead send over Hash
  $a12_headers['hash'] = a12_connect_hmac_hash($this->timestamp, $params['url'], $this->a12_key);
  return $a12_headers;
}

/**
 * Modify a solr base url and construct a hmac authenticator headers.
 *
 * @param $url
 *  The solr url beng requested - passed by reference and may be altered.
 * @param $string
 *  A string - the data to be authenticated, or empty to just use the path
 *  and query from the url to build the authenticator.
 * @param $derived_key
 *  Optional string to supply the derived key.
 * @param $schem
 *  Scheme to be used during the request
 *
 * @return
 *  An array containing the string to be added as the content of the
 *  Cookie header to the request.
 *
 */
function enterprise_search_auth_headers($url, $string = '', $scheme = NULL) {
  $uri = parse_url($url);
  if(is_null($scheme)) {
    // If no scheme is defined, default to ssl.
    if (in_array('ssl', stream_get_transports(), TRUE)) {
      $scheme = 'https://';
      $port = '';
    }
    else {
      $scheme = 'http://';
      $port = (isset($uri['port']) && $uri['port'] != 80) ? ':'. $uri['port'] : '';
    }
  }
  else {
    $port = ':' . $uri['port'];
  }

  $path = isset($uri['path']) ? $uri['path'] : '/';
  $query = isset($uri['query']) ? '?'. $uri['query'] : '';
  //$url = $scheme . $uri['host'] . $port . $path . $query;
  $params = array(
    'url' => $url,
    'timestamp' => time(),
  );
  $a12_conn = new a12_connector();
  $auth_header = $a12_conn->auth_headers($params);
  return $auth_header;
}

/**
 * Starts the timer with the specified name.
 *
 * If you start and stop the same timer multiple times, the measured intervals
 * will be accumulated.
 *
 * @param $name
 *   The name of the timer.
 */
function timer_start($name) {
  global $timers;

  $timers[$name]['start'] = microtime(TRUE);
  $timers[$name]['count'] = isset($timers[$name]['count']) ? ++$timers[$name]['count'] : 1;
}

/**
 * Reads the current timer value without stopping the timer.
 *
 * @param $name
 *   The name of the timer.
 *
 * @return
 *   The current timer value in ms.
 */
function timer_read($name) {
  global $timers;

  if (isset($timers[$name]['start'])) {
    $stop = microtime(TRUE);
    $diff = round(($stop - $timers[$name]['start']) * 1000, 2);

    if (isset($timers[$name]['time'])) {
      $diff += $timers[$name]['time'];
    }
    return $diff;
  }
  return $timers[$name]['time'];
}

/**
 * Performs an HTTP request.
 *
 * This is a flexible and powerful HTTP client implementation. Correctly
 * handles GET, POST, PUT or any other HTTP requests. Handles redirects.
 *
 * @param $url
 *   A string containing a fully qualified URI.
 * @param array $options
 *   (optional) An array that can have one or more of the following elements:
 *   - headers: An array containing request headers to send as name/value pairs.
 *   - method: A string containing the request method. Defaults to 'GET'.
 *   - data: A string containing the request body, formatted as
 *     'param=value&param=value&...'. Defaults to NULL.
 *   - max_redirects: An integer representing how many times a redirect
 *     may be followed. Defaults to 3.
 *   - timeout: A float representing the maximum number of seconds the function
 *     call may take. The default is 30 seconds. If a timeout occurs, the error
 *     code is set to the HTTP_REQUEST_TIMEOUT constant.
 *   - context: A context resource created with stream_context_create().
 *
 * @return object
 *   An object that can have one or more of the following components:
 *   - request: A string containing the request body that was sent.
 *   - code: An integer containing the response status code, or the error code
 *     if an error occurred.
 *   - protocol: The response protocol (e.g. HTTP/1.1 or HTTP/1.0).
 *   - status_message: The status message from the response, if a response was
 *     received.
 *   - redirect_code: If redirected, an integer containing the initial response
 *     status code.
 *   - redirect_url: If redirected, a string containing the URL of the redirect
 *     target.
 *   - error: If an error occurred, the error message. Otherwise not set.
 *   - headers: An array containing the response headers as name/value pairs.
 *     HTTP header names are case-insensitive (RFC 2616, section 4.2), so for
 *     easy access the array keys are returned in lower case.
 *   - data: A string containing the response body that was received.
 */
function drupal_http_request($url, array $options = array()) {
  // Allow an alternate HTTP client library to replace Drupal's default
  // implementation.
  $override_function = variable_get('drupal_http_request_function', FALSE);
  if (!empty($override_function) && function_exists($override_function)) {
    return $override_function($url, $options);
  }

  $result = new \stdClass();

  // Parse the URL and make sure we can handle the schema.
  $uri = @parse_url($url);

  if ($uri == FALSE) {
    $result->error = 'unable to parse URL';
    $result->code = -1001;
    return $result;
  }

  if (!isset($uri['scheme'])) {
    $result->error = 'missing schema';
    $result->code = -1002;
    return $result;
  }

  timer_start(__FUNCTION__);

  // Merge the default options.
  $options += array(
    'headers' => array(),
    'method' => 'GET',
    'data' => NULL,
    'max_redirects' => 3,
    'timeout' => 30.0,
    'context' => NULL,
  );

  // Merge the default headers.
  $options['headers'] += array(
    'User-Agent' => 'Drupal (+http://drupal.org/)',
  );

  // stream_socket_client() requires timeout to be a float.
  $options['timeout'] = (float) $options['timeout'];

  // Use a proxy if one is defined and the host is not on the excluded list.
  $proxy_server = variable_get('proxy_server', '');
  if ($proxy_server && _drupal_http_use_proxy($uri['host'])) {
    // Set the scheme so we open a socket to the proxy server.
    $uri['scheme'] = 'proxy';
    // Set the path to be the full URL.
    $uri['path'] = $url;
    // Since the URL is passed as the path, we won't use the parsed query.
    unset($uri['query']);

    // Add in username and password to Proxy-Authorization header if needed.
    if ($proxy_username = variable_get('proxy_username', '')) {
      $proxy_password = variable_get('proxy_password', '');
      $options['headers']['Proxy-Authorization'] = 'Basic ' . base64_encode($proxy_username . (!empty($proxy_password) ? ":" . $proxy_password : ''));
    }
    // Some proxies reject requests with any User-Agent headers, while others
    // require a specific one.
    $proxy_user_agent = variable_get('proxy_user_agent', '');
    // The default value matches neither condition.
    if ($proxy_user_agent === NULL) {
      unset($options['headers']['User-Agent']);
    }
    elseif ($proxy_user_agent) {
      $options['headers']['User-Agent'] = $proxy_user_agent;
    }
  }

  switch ($uri['scheme']) {
    case 'proxy':
      // Make the socket connection to a proxy server.
      $socket = 'tcp://' . $proxy_server . ':' . variable_get('proxy_port', 8080);
      // The Host header still needs to match the real request.
      $options['headers']['Host'] = $uri['host'];
      $options['headers']['Host'] .= isset($uri['port']) && $uri['port'] != 80 ? ':' . $uri['port'] : '';
      break;

    case 'http':
    case 'feed':
      $port = isset($uri['port']) ? $uri['port'] : 80;
      $socket = 'tcp://' . $uri['host'] . ':' . $port;
      // RFC 2616: "non-standard ports MUST, default ports MAY be included".
      // We don't add the standard port to prevent from breaking rewrite rules
      // checking the host that do not take into account the port number.
      $options['headers']['Host'] = $uri['host'] . ($port != 80 ? ':' . $port : '');
      break;

    case 'https':
      // Note: Only works when PHP is compiled with OpenSSL support.
      $port = isset($uri['port']) ? $uri['port'] : 443;
      $socket = 'ssl://' . $uri['host'] . ':' . $port;
      $options['headers']['Host'] = $uri['host'] . ($port != 443 ? ':' . $port : '');
      break;

    default:
      $result->error = 'invalid schema ' . $uri['scheme'];
      $result->code = -1003;
      return $result;
  }

  if (empty($options['context'])) {
    $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout']);
  }
  else {
    // Create a stream with context. Allows verification of a SSL certificate.
    $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout'], STREAM_CLIENT_CONNECT, $options['context']);
  }

  // Make sure the socket opened properly.
  if (!$fp) {
    // When a network error occurs, we use a negative number so it does not
    // clash with the HTTP status codes.
    $result->code = -$errno;
    $result->error = trim($errstr) ? trim($errstr) : t('Error opening socket @socket', array('@socket' => $socket));

    // Mark that this request failed. This will trigger a check of the web
    // server's ability to make outgoing HTTP requests the next time that
    // requirements checking is performed.
    // See system_requirements().
    die('http_request_failed');

    return $result;
  }

  // Construct the path to act on.
  $path = isset($uri['path']) ? $uri['path'] : '/';
  if (isset($uri['query'])) {
    $path .= '?' . $uri['query'];
  }

  // Only add Content-Length if we actually have any content or if it is a POST
  // or PUT request. Some non-standard servers get confused by Content-Length in
  // at least HEAD/GET requests, and Squid always requires Content-Length in
  // POST/PUT requests.
  $content_length = strlen($options['data']);
  if ($content_length > 0 || $options['method'] == 'POST' || $options['method'] == 'PUT') {
    $options['headers']['Content-Length'] = $content_length;
  }

  // If the server URL has a user then attempt to use basic authentication.
  if (isset($uri['user'])) {
    $options['headers']['Authorization'] = 'Basic ' . base64_encode($uri['user'] . (isset($uri['pass']) ? ':' . $uri['pass'] : ''));
  }

  // If the database prefix is being used by SimpleTest to run the tests in a copied
  // database then set the user-agent header to the database prefix so that any
  // calls to other Drupal pages will run the SimpleTest prefixed database. The
  // user-agent is used to ensure that multiple testing sessions running at the
  // same time won't interfere with each other as they would if the database
  // prefix were stored statically in a file or database variable.
  $test_info = &$GLOBALS['drupal_test_info'];
  if (!empty($test_info['test_run_id'])) {
    $options['headers']['User-Agent'] = drupal_generate_test_ua($test_info['test_run_id']);
  }

  $request = $options['method'] . ' ' . $path . " HTTP/1.0\r\n";
  foreach ($options['headers'] as $name => $value) {
    $request .= $name . ': ' . trim($value) . "\r\n";
  }
  $request .= "\r\n" . $options['data'];
  $result->request = $request;
  // Calculate how much time is left of the original timeout value.
  $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
  if ($timeout > 0) {
    stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
    fwrite($fp, $request);
  }

  // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
  // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
  // instead must invoke stream_get_meta_data() each iteration.
  $info = stream_get_meta_data($fp);
  $alive = !$info['eof'] && !$info['timed_out'];
  $response = '';

  while ($alive) {
    // Calculate how much time is left of the original timeout value.
    $timeout = $options['timeout'] - timer_read(__FUNCTION__) / 1000;
    if ($timeout <= 0) {
      $info['timed_out'] = TRUE;
      break;
    }
    stream_set_timeout($fp, floor($timeout), floor(1000000 * fmod($timeout, 1)));
    $chunk = fread($fp, 1024);
    $response .= $chunk;
    $info = stream_get_meta_data($fp);
    $alive = !$info['eof'] && !$info['timed_out'] && $chunk;
  }
  fclose($fp);

  if ($info['timed_out']) {
    $result->code = HTTP_REQUEST_TIMEOUT;
    $result->error = 'request timed out';
    return $result;
  }
  // Parse response headers from the response body.
  // Be tolerant of malformed HTTP responses that separate header and body with
  // \n\n or \r\r instead of \r\n\r\n.
  list($response, $result->data) = preg_split("/\r\n\r\n|\n\n|\r\r/", $response, 2);
  $response = preg_split("/\r\n|\n|\r/", $response);

  // Parse the response status line.
  list($protocol, $code, $status_message) = explode(' ', trim(array_shift($response)), 3);
  $result->protocol = $protocol;
  $result->status_message = $status_message;

  $result->headers = array();

  // Parse the response headers.
  while ($line = trim(array_shift($response))) {
    list($name, $value) = explode(':', $line, 2);
    $name = strtolower($name);
    if (isset($result->headers[$name]) && $name == 'set-cookie') {
      // RFC 2109: the Set-Cookie response header comprises the token Set-
      // Cookie:, followed by a comma-separated list of one or more cookies.
      $result->headers[$name] .= ',' . trim($value);
    }
    else {
      $result->headers[$name] = trim($value);
    }
  }

  $responses = array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Time-out',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Large',
    415 => 'Unsupported Media Type',
    416 => 'Requested range not satisfiable',
    417 => 'Expectation Failed',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Time-out',
    505 => 'HTTP Version not supported',
  );
  // RFC 2616 states that all unknown HTTP codes must be treated the same as the
  // base code in their class.
  if (!isset($responses[$code])) {
    $code = floor($code / 100) * 100;
  }
  $result->code = $code;

  switch ($code) {
    case 200: // OK
    case 304: // Not modified
      break;
    case 301: // Moved permanently
    case 302: // Moved temporarily
    case 307: // Moved temporarily
      $location = $result->headers['location'];
      $options['timeout'] -= timer_read(__FUNCTION__) / 1000;
      if ($options['timeout'] <= 0) {
        $result->code = HTTP_REQUEST_TIMEOUT;
        $result->error = 'request timed out';
      }
      elseif ($options['max_redirects']) {
        // Redirect to the new location.
        $options['max_redirects']--;
        $result = drupal_http_request($location, $options);
        $result->redirect_code = $code;
      }
      if (!isset($result->redirect_url)) {
        $result->redirect_url = $location;
      }
      break;
    default:
      $result->error = $status_message;
  }

  return $result;
}

/**
 * Encode special characters in a plain-text string for display as HTML.
 *
 * Also validates strings as UTF-8 to prevent cross site scripting attacks on
 * Internet Explorer 6.
 *
 * @param $text
 *   The text to be checked or processed.
 * @return
 *   An HTML safe version of $text, or an empty string if $text is not
 *   valid UTF-8.
 *
 * @see drupal_validate_utf8().
 */
function check_plain($text) {
  static $php525;

  if (!isset($php525)) {
    $php525 = version_compare(PHP_VERSION, '5.2.5', '>=');
  }
  // We duplicate the preg_match() to validate strings as UTF-8 from
  // drupal_validate_utf8() here. This avoids the overhead of an additional
  // function call, since check_plain() may be called hundreds of times during
  // a request. For PHP 5.2.5+, this check for valid UTF-8 should be handled
  // internally by PHP in htmlspecialchars().
  // @see http://www.php.net/releases/5_2_5.php
  // @todo remove this when support for either IE6 or PHP < 5.2.5 is dropped.

  if ($php525) {
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
  }
  return (preg_match('/^./us', $text) == 1) ? htmlspecialchars($text, ENT_QUOTES, 'UTF-8') : '';
}

  /**
   * Central method for making the actual http request to the Solr Server
   *
   * This is just a wrapper around drupal_http_request().
   */
  function _makeHttpRequest_parent($url, array $options = array()) {
    if (!isset($options['method']) || $options['method'] == 'GET' || $options['method'] == 'HEAD') {
      // Make sure we are not sending a request body.
      $options['data'] = NULL;
    }

    $result = drupal_http_request($url, $options);

    if (!isset($result->code) || $result->code < 0) {
      $result->code = 0;
      $result->status_message = 'Request failed';
      $result->protocol = 'HTTP/1.0';
    }
    // Additional information may be in the error property.
    if (isset($result->error)) {
      $result->status_message .= ': ' . check_plain($result->error);
    }

    if (!isset($result->data)) {
      $result->data = '';
      $result->response = NULL;
    }
    else {
      $response = json_decode($result->data);
      if (is_object($response)) {
        foreach ($response as $key => $value) {
          $result->$key = $value;
        }
      }
    }
    return $result;
  }


/**
 * Override _makeHttpRequest
 * Attach authentication headers and check the response for errors
 */
function _makeHttpRequest($url, array $options = array()) {
  $options['headers'] = enterprise_search_auth_headers($url);
  $options['User-Agent'] = 'a12_find/'. A12_FIND_VERSION;
  $response = _makeHttpRequest_parent($url, $options);
  if(isset($response->status) && $response->status == "error") {
    print_r($response);
  }
  return $response;
}

/**
 * Check the reponse code and thow an exception if it's not 200.
 *
 * @param stdClass $response
 *   response object.
 *
 * @return
 *  response object
 * @thows Exception
 */
function checkResponse($response) {
  $code = (int) $response->code;
  if ($code != 200) {
    if ($code >= 400 && $code != 403 && $code != 404) {
      // Add details, like Solr's exception message.
      $response->status_message .= $response->data;
    }
    print_r('"' . $code . '" Status: ' . $response->status_message);
  }
  return $response;
}

/**
 * Return a valid http URL given this server's host, port and path and a provided servlet name
 *
 * @param $servlet
 *  A string path to a Solr request handler.
 * @param $params
 * @param $parsed_url
 *   A url to use instead of the stored one.
 *
 * @return string
 */
function _constructUrl($servlet, $params = array(), $added_query_string = NULL) {
  // PHP's built in http_build_query() doesn't give us the format Solr wants.
  $query_string = httpBuildQuery($params);

  if ($query_string) {
    $query_string = '?' . $query_string;
    if ($added_query_string) {
      $query_string = $query_string . '&' . $added_query_string;
    }
  }
  elseif ($added_query_string) {
    $query_string = '?' . $added_query_string;
  }

  $url = parse_url('https://find.axis12.com/');
  //return $url['scheme'] . $url['user'] . $url['pass'] . $url['host'] . $url['port'] . $url['path'] . $servlet . $query_string;
  return $url['scheme'] .'://'. $url['host'] . $url['path'] . $servlet . $query_string;
}

function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

/**
 * Like PHP's built in http_build_query(), but uses rawurlencode() and no [] for repeated params.
 */
function httpBuildQuery(array $query, $parent = '') {
  $params = array();

  foreach ($query as $key => $value) {
    $key = ($parent ? $parent : rawurlencode($key));

    // Recurse into children.
    if (is_array($value)) {
      $params[] = httpBuildQuery($value, $key);
    }
    // If a query parameter value is NULL, only append its key.
    elseif (!isset($value)) {
      $params[] = $key;
    }
    else {
      $params[] = $key . '=' . rawurlencode($value);
    }
  }

  return implode('&', $params);
}


/**
 * Central method for making a GET operation against this Solr Server
 */
function _sendRawGet($url, $options = array()) {
  $response = _makeHttpRequest($url, $options);
  return checkResponse($response);
}

class a12_connector {

  /**
   * Defined unique identifier for connecting to A12 webservices framework
   * @var mixed
   */
  protected $a12_identifier;

  /**
   * Defined A12 Key for this website
   * @var mixed
   */
  protected $a12_key;

  /**
   * Defined address of A12 network host service
   * @var mixed
   */
  protected $a12_network_address;

  /**
   * Defined status of the website within A12 network
   * @var array
   */
  protected $a12_subscription_data;

  /**
   * Defined timestamp when the connector was prepared to call server
   * @var timestamp
   */
  protected $a12_timestamp;

  /**
   * Defined status if the current website has a valid subscription 
   * @var bool
   */
  public $has_subscription_info;

  /**
   * Defined status of the subscription
   * @var bool, 
   */
  public $is_active;

  /**
   * Constructor
   * @param array $params
   */
  public function __construct(array $params = array(), $action = FALSE){
    if (!empty($params) && $action == A12_SUBSCRIPTION_ACTION_SAVE){
      $this->set_vars($params);
    }

    $this->a12_identifier         = key_exists('a12_identifier', $params) ? $params['a12_identifier'] : variable_get('a12_identifier', FALSE);
    $this->a12_key                = key_exists('a12_key', $params) ? $params['a12_key'] : variable_get('a12_key', FALSE) ;
    $this->a12_network_address    = variable_get('a12_network_address', A12_VALIDATE_METHOD);
    $this->a12_subscription_data  = variable_get('a12_subscription_data', FALSE);
    $this->has_subscription_info = $this->has_subscription_credentials();
    $this->is_active = $this->is_active_subscription();
    $this->timestamp = REQUEST_TIME;
   }
  
  /**
   * sets values of all variables, donot use directly. Use as part of creating a new a12_connector object
   * @param array $params
   */
  protected function set_vars(array $params = array()){
    foreach($params as $a12_var_id => $val)
      variable_set($a12_var_id, $val);
  }

  
 /**
 * returns array of A12 connect variables that will allow sites to communicate with A12 webservices
 * @return mixed
 */
  private function connect_variables($params) {
    $this->a12_identifier         = key_exists('a12_identifier', $params) ? $params['a12_identifier'] : variable_get('a12_identifier', FALSE);
    $this->a12_key                = key_exists('a12_key', $params) ? $params['a12_key'] : variable_get('a12_key', FALSE) ;
    $this->a12_network_address    = variable_get('a12_network_address', 'https://services.axis12.com');
    $this->a12_subscription_data  = variable_get('a12_subscription_data', FALSE);
  }

  /**
   * Check if required key and identifier have been set
   * @return bool
   */
  public function has_subscription_credentials() {
    return (bool)(variable_get('a12_identifier', 'CQ-94987749-704859') && variable_get('a12_key', '85aa7b8a4a42e451302264c064869dcb'));
  }


  public function is_active_subscription() {
    $active = FALSE;
    // Subscription cannot be active if we have no credentials.
    $active = (!empty($this->has_subscription_info) && !empty($this->a12_subscription_data['active']))? TRUE : FALSE;
    return TRUE;
  }

  /*
   * Register a product with A12 network
   * @param a12product $module
   */
  function register(a12product $module) {

  }

  /**
   * Delete stored settings
   * @return
   */
  public static function delete_settings(){
    variable_del('a12_key');
    variable_del('a12_identifier');
    return drupal_set_message('Settings deleted');
  }

  /**
   * Performs XMLRPC call to A12services to identify if a given user identifier and key are valid
   */
  function validate_credentials() {
    global $base_url;
    $a12_network_address = A12_WEBSERVICES_URL . "/xmlrpc.php  ";
    $data = array(
      'auth_headers'  => $this->auth_headers(array('url' => $base_url)),
      'host'          => $host = isset($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_ADDR"] : '',
    );
    $result = a12_connect_send_xmlrpc_request($a12_network_address, A12_VALIDATE_METHOD, $data);
    return $result;
  }

  /**
   * Prepares headers that shall be used by A12 webservices to validate
   * incoming requests
   *
   * @param aray $params
   * Any additional paramters that are to be used in preparing headers
   *
   * @return array
   */
  public function auth_headers($params = array()) {
    global $base_url;
    $a12_headers = array();
    $a12_headers['timestamp'] = $this->timestamp;
    $a12_headers['identifier'] = $this->a12_identifier;
    $a12_headers['site_url'] = $base_url;
    $a12_headers['site_name'] = $_SERVER['SERVER_NAME'];
    $a12_headers['ip'] = $_SERVER['SERVER_ADDR'];
    // Although we can always send the key to XMLRPC request as
    // $a12_headers['key'] = $this->a12_key;
    // We avoid sending key over and instead send over Hash
    $a12_headers['hash'] = a12_connect_hmac_hash($this->timestamp, $params['url'], $this->a12_key);
    return $a12_headers;
  }

}

/**
 * Subscription class responsible for all subscriptions and updates.
 */
class a12_subscription extends a12_connector {
  function  __construct(array $params = array()) {
    parent::__construct($params);
  }
}

/**
 * A12 Find adapter
 *
 */
class A12Find extends Configurable implements AdapterInterface
{

    /**
     * Initialization hook
     *
     * @throws RuntimeException
     */
    protected function init()
    {
        // @codeCoverageIgnoreEnd
    }

    /**
     * Execute a Solr request
     *
     * @param  Request  $request
     * @param  Endpoint $endpoint
     * @return Response
     */
    public function execute($request, $endpoint)
    {
        return $this->getData($request, $endpoint);
    }

    /**
     * Execute request
     *
     * @param  Request  $request
     * @param  Endpoint $endpoint
     * @return Response
     */
    protected function getData($request, $endpoint)
    {      
        // @codeCoverageIgnoreStart
        $uri = A12_TMP_DEFAULT_SCHEME . A12_FIND_SERVER .  $endpoint->getCore() . $request->getUri();   
        $httpResponse = _sendRawGet($uri);
        return $this->getResponse($httpResponse->data);
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get the response
     *
     * @param  string   $httpResponse
     * @return Response
     */
    public function getResponse($httpResponse)
    {
        // @codeCoverageIgnoreStart
        if ($httpResponse !== false) {
            $data = $httpResponse;
            $headers = array();
            $headers[] = 'HTTP/1.1 200 OK';
        } else {
            $headers = array();
            $data = '';
        }

        return new Response($data, $headers);
        // @codeCoverageIgnoreEnd
    }

}
